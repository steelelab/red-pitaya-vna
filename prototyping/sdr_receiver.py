import struct
import numpy as np


class sdrReceiver():
    
    def __init__(self, ipaddr):
        '''
        Creates a reciever class for reading data from the SDR transciever on the red pitata 
        (https://pavel-demin.github.io/red-pitaya-notes/sdr-transceiver/)
        
        Before using, make sure SDR transciever app is running on the RP, for example by
        opening this link:
        
        http://192.168.1.100/sdr_transceiver
        
        ipaddr should be the IP address of the RP, by default configured as 192.168.1.100 
        on the sd card
        
        Contains some attributes initialized as:
        
        frequency (10e6 = 10 MHz)
        rate (index from 0 to 5 for 20/50/100/250/500/1250 kSa/sec, see rate_options dictionary)
        
        functions: 
        
        
        ''' 
        self.p_ip_address = ipaddr
        self.control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.frequency = 10e6
        self.rate_options = {20000:0, 50000:1, 100000:2, 250000:3, 500000:4, 1250000:5}
        self.rate = 0
        
    def close_socket(self):
        self.data_socket.close()
        self.control_socket.close()
        
    def __del__(self):
        self.close_socket()

    
