---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.15.2
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Parsing binary data

Starting point: use `remote_rx.py` to save data to a file:

```
python remote_rx.py --address 192.168.1.100 --rate 50000 --freq 1410000
...receiving at: 1410000 Hz sample sample rate: 50000  / correction (ppm): 0
c^Cshutdown
bye
```

I modified the code so that it would save to a file test.raw

```python
import struct
import numpy as np
import matplotlib.pyplot as plt
```

```python
!ls -s test.raw
```

```python
with open("test.raw", "rb") as f:
    file_contents = f.read()
```

```python
N = 100
file_contents[0:4*N]
```

```python
test = np.array(struct.unpack("<100f", file_contents[0:4*N]))
```

```python
I = test[0::2]
Q = test[1::2]
t = np.array(range(N//2))*1/50e3
```

```python
plt.plot(t,I)
plt.plot(t,Q)
```

Seems reasonable. Now real test is to repeat this while sending in a signal from the WFT close to the demodulation frequency and check that we get a cosine.


# Now load straight from SDR server into python

First, we'll try getting some data in directly into this notebook. Start by adapting verbatim.

```python
import socket, struct, signal, traceback, time
import struct
import numpy as np
import matplotlib.pyplot as plt

N = 1024

rate_index = {20000:0, 50000:1, 100000:2, 250000:3, 500000:4, 1250000:5}
freq = 10e6
rate = 100000
ip = "192.168.1.100"

def get_data():
    control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Both read and write are both on port 1001 (not sure why we use two different sockets...?)
    control_socket.connect((ip, 1001))
    data_socket.connect((ip,1001))
    
    control_socket.send(struct.pack('<I', 0))
    data_socket.send(struct.pack('<I', 1))
    
    # Set freq and rate
    control_socket.send(struct.pack('<I', 0<<28 | int(freq)))
    control_socket.send(struct.pack('<I', 1<<28 | rate_index[rate]))

    bytesRead = 0
    bytesToGo = N*2*4
    while bytesRead < N*2*4:
        try:
            if bytesRead == 0:
                raw_data = data_socket.recv(bytesToGo) # 32 bit = 4 bytes, then x2 for I and Q
            else:
                raw_data += data_socket.recv(bytesToGo) # 32 bit = 4 bytes, then x2 for I and Q
            #print(len(raw_data))
            bytesToGo = N*2*4-len(raw_data)
            bytesRead = len(raw_data)
        except KeyboardInterrupt:
            data_socket.close()
            control_socket.close()
            return "","",""
        except:
            traceback.print_exc()
            data_socket.close()
            control_socket.close()            
                        
    data_socket.close()
    control_socket.close()
    
    Nrecv = len(raw_data)
    Ncomp = (len(raw_data)//4//2)
    
    # print("Requested bytes: %d" % (N*4*2)) 
    # print("Recieved bytes: %d" % Nrecv)
    # print("Requested IQs:  %d" % N)
    # print("Completed IQs:  %d" % Ncomp) 
    
    data = np.array(struct.unpack("<%df" % (Ncomp*2), raw_data))
    I = data[0::2]
    Q = data[1::2]
    t = np.array(range(Ncomp))*1/rate
    return I, Q, t
```

```python
get_data()
```

```python
import numpy as np

from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
output_notebook()
import ipywidgets as widgets
```

```python
I,Q,t = get_data()
p = figure(height=200, width=600) 
p.sizing_mode = "scale_width"
l1 = p.line(t,I,color='blue')
l2 = p.line(t,Q,color='red')
target = show(p, notebook_handle=True)
```

A hacked live viewer:

```python
print("live view started")
while True:
    try: 
        I,Q,t = get_data()
        if type(I) is not np.ndarray:
            break
        l1.data_source.data = dict(x=t,y=I)
        l2.data_source.data = dict(x=t,y=Q)
        time.sleep(0.1)
        push_notebook(handle=target)
    except KeyboardInterrupt:
        print("done")
        break
```

Sometimes a bit flaky, I think bokeh crashes something and then I can no longer get traces from the server. Curiously, I need to reload the browser tab and then it works again, even without restarting the kernel...

```python
import wft
```

```python
port = "/dev/tty.usbmodem206C34714E561"
wft1 = wft.WFT(port)
```

```python
wft1.query("f?")
```

```python
I,Q,t = get_data()
plt.figure(figsize=(12,4))
plt.plot(t,I)
plt.plot(t,Q)
plt.plot(t,np.sin(2*np.pi*2e3*t)*0.05-0.25, c='grey', ls=":")
```

Confirmed: things work! The IQs oscillate at 2 kHz. Actually, if I look closely, I can see that these are not clocked properly, they are slowly drifting. That is maybe already my need for a clock lock between the RP and the WFT.

How far off? Maybe 20 oscillations to slip? that would be 100 Hz on 10 MHz, 1 part in 10^4. Maybe not unexpected for the absolute accuracy of the RP clock frequency? 


## Figuring out how to get both inputs


Seems to be that you can create a separate set of sockets on port 1002 to get input 2.

```python
import socket, struct, signal, traceback, time

N = 1024

rate_index = {20000:0, 50000:1, 100000:2, 250000:3, 500000:4, 1250000:5}
freq = 10e6
rate = 100000
ip = "192.168.1.100"

def get_data2():
    control_socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    data_socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    control_socket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    data_socket2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    

    # Both read and write are both on port 1001 (not sure why we use two different sockets...?)
    control_socket1.connect((ip, 1001))
    data_socket1.connect((ip,1001))

    control_socket2.connect((ip, 1002))
    data_socket2.connect((ip,1002))

    control_socket1.send(struct.pack('<I', 0))
    data_socket1.send(struct.pack('<I', 1))

    control_socket2.send(struct.pack('<I', 0))
    data_socket2.send(struct.pack('<I', 1))

    # Set freq and rate
    control_socket1.send(struct.pack('<I', 0<<28 | int(freq)))
    control_socket1.send(struct.pack('<I', 1<<28 | rate_index[rate]))
    control_socket2.send(struct.pack('<I', 0<<28 | int(freq)))
    control_socket2.send(struct.pack('<I', 1<<28 | rate_index[rate]))


    bytesRead1 = 0
    bytesToGo1 = N*2*4
    bytesRead2 = 0
    bytesToGo2 = N*2*4
    while bytesRead1 < N*2*4 or bytesRead2 < N*2*4:
            if bytesRead1 == 0:
                raw_data1 = data_socket1.recv(bytesToGo1) # 32 bit = 4 bytes, then x2 for I and Q
            elif bytesRead1 < N*2*4:
                raw_data1 += data_socket1.recv(bytesToGo1) # 32 bit = 4 bytes, then x2 for I and Q
            bytesToGo1 = N*2*4-len(raw_data1)
            bytesRead1 = len(raw_data1)

            if bytesRead2 == 0:
                raw_data2 = data_socket2.recv(bytesToGo2) # 32 bit = 4 bytes, then x2 for I and Q
            elif bytesRead2 < N*2*4:
                raw_data2 += data_socket2.recv(bytesToGo2) # 32 bit = 4 bytes, then x2 for I and Q
            #print(len(raw_data))
            bytesToGo2 = N*2*4-len(raw_data2)
            bytesRead2 = len(raw_data2)    

    data_socket1.close()
    control_socket1.close()
    data_socket2.close()
    control_socket2.close()

    data1 = np.array(struct.unpack("<%df" % (N*2), raw_data1))
    I1 = data1[0::2]
    Q1 = data1[1::2]
    data2 = np.array(struct.unpack("<%df" % (N*2), raw_data2))
    I2 = data2[0::2]
    Q2 = data2[1::2]
    t = np.array(range(N))*1/rate
    return I1, Q1, I2, Q2, t
```

We'll also start to make this notebook into a logbook. 

```python
import datetime
from IPython.display import Image
def save_plot():
    filename = datetime.datetime.now().strftime("%Y_%m_%d_%H.%M.%S")+".png"
    plt.savefig(filename)
    Image(filename)
    print(filename)
```

```python
I1, Q1, I2, Q2, t = get_data2()
plt.figure(figsize=(12,6))
plt.subplot(211)
plt.plot(t,I1)
plt.plot(t,Q1)
plt.subplot(212)
plt.plot(t,I2)
plt.plot(t,Q2)
save_plot()
```

OK, data acquisition seems to work!

We see here maybe already a problem: leakage into the other channel. The plot 2023_11_23_06.47.33.png is without the other channel connected. Maybe things will get better if we 50 ohm terminate the RP inputs.

Actually, probably best is to buy one of these, they are 50 ohm terminated already (and 16 bit):

https://redpitaya.com/sdrlab-122-16/

> It comes with two 16-bit 50-ohm inputs and 14-bit 50-ohm outputs, Xilinx Zynq 7020 FPGA for real-time processing capabilities plus an ultra-low phase noise 122.88MHz clock which makes it more hardware-compatible with HPSDR compliant applications. RF inputs are optimized for minimal distortion, noise and crosstalk which significantly improves reception and broadens the choice of antenna.

Yep, for sure. Time to order.

Only thing I'm not 100% sure on is the external clock. I think the problem is going to be that the RPs have no support for a 10 MHz external ref. They do have an external clock input I believe. 

But you need to buy a special version of the board (or manually desolder a bunch of components). And you need to generate the (122 or 125 MHz) clock, I guess derived from the 10 MHz system clock.

Unless I can clock the WFT with a 122 MHz external ref, in which case we should be good. Then I could probably pick up the clock out of RP and use it as ref in on the WFT. But it goes only to 100 MHz...

I guess probably a divide by 2 circuit would work. Hmm.

Or I could just use the RP as the clock using its DDS to generate an output ref on one of the unused outputs. Probalbly best for now. 
